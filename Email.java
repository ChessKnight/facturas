
public class Email {
	
	private static final String VALIDACION="^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	private static final int LONGITUD=100;
	private String email;
	
	Email(String email) throws Exception{
		if(email!=null && email.matches(VALIDACION) && email.length()<=LONGITUD)
			this.email=email;
		else
			throw new Exception("Email no valido");
	}
	
	
}

