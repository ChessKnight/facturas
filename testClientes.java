
public class testClientes {
	
	public static void main (String[] args) throws Exception{
		DatosClientes datos=new DatosClientes();
		System.out.println(datos.guardar(new Cliente(new Dni("12345678X"), new Nombre("Luis"), new Apellidos("Ferrer"))));
		System.out.println(datos.guardar(new Cliente(new Dni("12345678A"), new Nombre("Nicolas"), new Apellidos("Manero"))));
		if(datos.cambiarTelefono(new Dni("12345678X"), new Telefono(692847583)))
			System.out.println("Telefono cambiado correctamente");
		else
			System.out.println("No se ha podido cambiar el telefono");
		
		System.out.println(datos);
		
		System.out.println();
		System.out.println(datos.borrar(new Dni("12345678X")));
		System.out.println();
		System.out.println(datos);
		System.out.println();
		System.out.println(datos.guardar(new Cliente(new Dni("12345678X"), new Nombre("Luis"), new Apellidos("Ferrer"))));
		System.out.println(datos.guardar(new Cliente(new Dni("12345678X"), new Nombre("Luis"), new Apellidos("Ferrer"))));
		System.out.println(datos);
		System.out.println();
		datos.borrarTodo();
		System.out.println(datos);
		
	}
}

