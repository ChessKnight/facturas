
public class Direccion {
	
	private static final int LONGITUD=100;
	private String direccion;
	
	Direccion(String direccion) throws Exception{
		if(direccion.length()<=LONGITUD)
			this.direccion=direccion;
		else
			throw new Exception("Direccion no valida");
	}
	
}

