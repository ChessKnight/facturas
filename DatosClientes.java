import java.util.ArrayList;

public class DatosClientes {
	
	private ArrayList<Cliente> datos=new ArrayList<Cliente>();
	
	public String guardar(Cliente c){
		boolean temp=false;
		for(int i=0; i<datos.size(); i++)
			if(c.getDni().equals(datos.get(i).getDni()))
				temp=true;
		
		if(temp){
			return "DNI repetido, no se ha guardado el cliente";
		}else{
			datos.add(c);
			return "Se ha guardado el cliente";
		}
	}
	
	public Cliente leer(Dni dni){
		for(int i=0; i<datos.size(); i++)
			if(dni.getDni().equals(datos.get(i).getDni()))
				return datos.get(i);
		return null;
	}
	
	public String borrar(Dni dni){
		boolean resultado=false;
		for(int i=0; i<datos.size(); i++){
			if(dni.getDni().equals(datos.get(i).getDni())){
				datos.remove(i);
				resultado=true;
			}
		}
		if(resultado)
			return "Se ha podido borrar el cliente";
		else
			return "No he sa podido borrar el cliente";
	}
	
	public void borrarTodo(){
		datos.clear();
	}

	public boolean cambiarDireccion(Dni dni, Direccion direccion){
		boolean resultado=false;
		for(int i=0; i<datos.size(); i++){
			if(dni.getDni().equals(datos.get(i).getDni())){
				datos.get(i).setDireccion(direccion);
				resultado=true;
			}
		}
		return resultado;
	}

	public boolean cambiarTelefono(Dni dni, Telefono telefono){
		boolean resultado=false;
		for(int i=0; i<datos.size(); i++){
			if(dni.getDni().equals(datos.get(i).getDni())){
				datos.get(i).setTelefono(telefono);
				resultado=true;
			}
		}
		return resultado;
	}

	public boolean cambiarEmail(Dni dni, Email email){
		boolean resultado=false;
		for(int i=0; i<datos.size(); i++){
			if(dni.getDni().equals(datos.get(i).getDni())){
				datos.get(i).setEmail(email);
				resultado=true;
			}
		}
		return resultado;
	}
	
	public String toString(){
		String resultado="";
		for(int i=0; i<datos.size(); i++)
			resultado=resultado+datos.get(i)+"\n";
		return resultado;
	}
	
}

