
public class Apellidos {
	
	private static final int LONGITUD=100;
	private String apellidos;
	
	Apellidos(String apellidos) throws Exception{
		if(apellidos.length()<=LONGITUD)
			this.apellidos=apellidos;
		else
			throw new Exception("Apellidos no validos");
	}
	
	public String getApellidos(){
		return apellidos;
	}
	
}

