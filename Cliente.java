
public class Cliente {
	
	private Dni dni;
	private Nombre nombre;
	private Apellidos apellidos;
	private Direccion direccion;
	private Telefono telefono;
	private Email email;
	
	Cliente(Dni dni, Nombre nombre, Apellidos apellidos){
		this.dni=dni;
		this.nombre=nombre;
		this.apellidos=apellidos;
	}
	
	public void setDireccion(Direccion direccion){
		this.direccion=direccion;
	}
	public void setTelefono(Telefono telefono){
		this.telefono=telefono;
	}
	public void setEmail(Email email){
		this.email=email;
	}
	
	public String getDni(){
		return this.dni.getDni();
	}
	
	public String toString(){
		return "Nombre: "+nombre.getNombre()+", Apellidos: "+apellidos.getApellidos();
	}
	
}

