
public class Nombre {
	
	private static final int LONGITUD=100;
	private String nombre;
	
	Nombre(String nombre) throws Exception{
		if(nombre.length()<=LONGITUD)
			this.nombre=nombre;
		else
			throw new Exception("Nombre no valido");
	}
	
	public String getNombre(){
		return nombre;
	}
	
}

