
public class Telefono {
	
	private int telefono;
	
	Telefono(int telefono) throws Exception{
		if(telefono>0 && String.valueOf(telefono).length()==9)
			this.telefono=telefono;
		else
			throw new Exception("Telefono no valido");
	}
	
}

