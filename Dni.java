
public class Dni {
	
	private static final String VALIDACION="[0-9]{8}+[A-Z]";
	private String dni;
	
	Dni(String dni) throws Exception{
		if(dni.matches(VALIDACION))
			this.dni=dni;
		else
			throw new Exception("DNI no valido");
	}
	
	public String getDni(){
		return dni;
	}
}

